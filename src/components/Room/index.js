import * as React from 'react'

document.getElementById("button_result").className = "btn btn-block btn-light";
document.getElementById('button_result').innerText = 'VS';
    function ambil_nilai(clicked_id)

		if(clicked_id==='rock')
		{
			document.getElementById('rock_player1').style.backgroundColor = "grey";
			document.getElementById('scissor_player1').style.backgroundColor = "";
			document.getElementById('paper_player1').style.backgroundColor = "";
			
        }
        else if(clicked_id==='scissor')
		{
			document.getElementById('rock_player1').style.backgroundColor = "";
			document.getElementById('scissor_player1').style.backgroundColor = "grey";
			document.getElementById('paper_player1').style.backgroundColor = "";
		}
		else if(clicked_id==='paper')
		{
			document.getElementById('rock_player1').style.backgroundColor = "";
			document.getElementById('scissor_player1').style.backgroundColor = "";
			document.getElementById('paper_player1').style.backgroundColor = "grey";
        }
        
        if(clicked_id==='rock' && descp==='rock')
		{
			var result="Draw";
		}
		else if (clicked_id==='rock' && descp==='scissor')
		{
			var result="Win";
		}
		else if (clicked_id==='rock' && descp==='paper')
		{
			var result="Lose";
		}
		else if (clicked_id==='scissor' && descp==='rock')
		{
			var result="Lose";
		}
		else if (clicked_id==='scissor' && descp==='scissor')
		{
			var result="Draw";
		}
		else if (clicked_id==='scissor' && descp==='paper')
		{
			var result="Win";
		}
		else if (clicked_id==='paper' && descp==='rock')
		{
			var result="Win";
		}
		else if (clicked_id==='paper' && descp==='scissor')
		{
			var result="Lose";
		}
		else if (clicked_id==='paper' && descp==='paper')
		{
			var result="Draw";
		}
		
		
		alert(clicked_id+' VS '+descp+'\r\nYou '+result);
		
		
		if(result==='Lose')
		{
			//document.getElementById("result_gambar").src = "assets/Lose.png";
            document.getElementById("button_result").className = "btn btn-sm btn-danger";
            document.getElementById('button_result').innerText = 'You Lose';
		}
		else if(result==='Win')
		{
			//document.getElementById("result_gambar").src = "assets/Win.png";
            document.getElementById("button_result").className = "btn btn-sm btn-success";
            document.getElementById('button_result').innerText = 'You Win';
		}
		else if(result==='Draw')
		{
			//document.getElementById("result_gambar").src = "assets/Draw.png";
            document.getElementById("button_result").className = "btn btn-sm btn-warning";
            document.getElementById('button_result').innerText = 'Draw';
		}
	
	function refresh(clicked_id)
	{
		//document.getElementById("result_gambar").src = "assets/VS.png";
        document.getElementById("button_result").className = "btn btn-block btn-light";
        document.getElementById('button_result').innerText = 'VS';
		
		///ini buat ngosongin warna pas kita pilih mulai main dari awal
		document.getElementById('rock_player1').style.backgroundColor = "";
		document.getElementById('scissor_player1').style.backgroundColor = "";
		document.getElementById('paper_player1').style.backgroundColor = "";
		document.getElementById('rock_player2').style.backgroundColor = "";
		document.getElementById('scissor_player2').style.backgroundColor = "";
		document.getElementById('paper_player2').style.backgroundColor = "";
	}